import unittest
from application import (
    application,
    get_all_cities,
    get_all_events,
    get_all_meetups,
    get_city,
    get_meetup,
    get_one_event,
)
import json

# todo use app_ctx with statement


class Test(unittest.TestCase):
    def test_get_all_events(self):
        with application.test_request_context(
            'events', data={}
        ):
            test_events = get_all_events()
            self.assertGreater(len(test_events), 0)

    def test_get_all_meetups(self):
        with application.test_request_context(
            'meetups', data={}
        ):
            test_meetups = get_all_meetups()
            self.assertGreater(len(test_meetups), 0)

    def test_get_all_cities(self):
        with application.test_request_context(
            'cities', data={}
        ):
            test_cities = get_all_cities()
            self.assertGreater(len(test_cities), 0)

    def test_get_city_found(self):
        with application.app_context():
            found_id = get_city("Philadelphia")
            self.assertNotEqual("Could not find city", found_id)

    def test_get_city_not_found(self):
        with application.app_context():
            try:
                not_found_id = get_city("ChIJLwPMoJm1RIYRetVp1EtGm12")
                assert 1 == 0
            except:
                pass

    def test_get_meetup_found(self):
        with application.app_context():
            found_id = get_meetup("21590254")
            self.assertNotEqual("Could not find meetup", found_id)

    def test_get_meetup_not_found(self):
        with application.app_context():
            try:
                not_found_id = get_meetup("31590254")
                assert 1 == 0
            except:
                pass

    def test_get_event_found(self):
        with application.app_context():
            found_id = get_one_event("G5dIZ4IS0oBc5")
            self.assertNotEqual("Could not find event", found_id)

    def test_get_event_not_found(self):
        with application.app_context():
            try:
                not_found_id = get_one_event("G5dIZ4IS0oBc6")
                assert 1 == 0
            except:
                pass


    # Filtering tests

    def test_filter_events(self):
        # app context fix
        filter_by = "category"
        matching = "Music"
        with application.test_request_context(
            '/events?' + filter_by + '=' + matching
        ):
            test_events = get_all_events()
            events = json.loads(test_events)
            for e in events:
                self.assertEqual(e[filter_by], matching)

    def test_filter_cities(self):
        # app context fix
        min_filter_by = "minPopulation"
        min_matching = 500_000
        max_filter_by = "maxPopulation"
        max_matching = 1_000_000
        with application.test_request_context(
            '/cities?' + min_filter_by + '=' + str(min_matching) + "&" + max_filter_by + "=" + str(max_matching)
        ):
            test_cities = get_all_cities()
            cities = json.loads(test_cities)
            for c in cities:
                self.assertGreaterEqual(c["population"], min_matching)
                self.assertLessEqual(c["population"], max_matching)

    def test_filter_meetups(self):
        # app context fix
        filter_by = "category"
        matching = "Singles"
        with application.test_request_context(
            '/meetups?' + filter_by + '=' + matching
        ):
            test_meetups = get_all_meetups()
            meetups = json.loads(test_meetups)
            for m in meetups:
                self.assertEqual(m[filter_by], matching)

if __name__ == "__main__":
    unittest.main()
