# Sort
comphrehensive list of what can be sorted

## Events
```
api.after-work.site/events?sortBy=<sorting parameter>
```
### Sorting Parameters
name  
start_date  
city_name  
category  
genre

## Cities
```
api.after-work.site/cities?sortBy=<sorting parameter>
```
### Sorting Parameters
name  
city_name  
status  
members  
category  
genre  
organizer_name

## Meetups
```
api.after-work.site/cities?sortBy=<sorting parameter>
```
### Sorting Parameters
name  
region  
elevationMeters  
population  
timezone  