from models import Event, Meetup, City
from collections import deque
from sqlalchemy import or_

def search(q):
    
    # needs to search all
    terms = q.split()
               #City    Event    Meetup
    results = [deque(), deque(), deque()]
    seen = [set(), set(), set()]
    print(q, terms)

    for term in terms:
        term = '%' + str(term) + '%'

        #City
        city_results = City.query.filter(City.name.ilike(term)).all()
        for r in city_results:
            if r.name not in seen[0]:
                seen[0].add(r.name)
                results[0].append(r.as_dict())
        
        
        #Event
        event_results = Event.query.filter(or_(Event.name.ilike(term),
                                               Event.category.ilike(term),
                                               Event.genre.ilike(term))).all()
        for r in event_results:
            if r.id not in seen[1]:
                seen[1].add(r.id)
                results[1].append(r.as_dict())

        #Meetup
        meetup_results = Meetup.query.filter(or_(Meetup.name.ilike(term),
                                                 Meetup.description.ilike(term),
                                                 Meetup.category.ilike(term),
                                                 Meetup.organizer_name.ilike(term))).all() 
        for r in meetup_results:
            if r.id not in seen[2]:
                seen[2].add(r.id)
                results[2].append(r.as_dict())

    for name in seen[0]:
       #Event
        event_results = Event.query.filter(Event.city_name.ilike(term)).all()
        for r in event_results:
            if r.id not in seen[1]:
                seen[1].add(r.id)
                results[1].append(r.as_dict())

        meetup_results = Meetup.query.filter(Meetup.city_name.ilike(term)).all() 
        #Meetup
        for r in meetup_results:
            if r.id not in seen[2]:
                seen[2].add(r.id)
                results[2].append(r.as_dict())
    results = [list(results[0]), list(results[1]), list(results[2])]

    #models = (City, Event, Meetup)
    #city_names = None
    #results = deque()
    #for i,m in enumerate(models):
    #    model_results = m.query.filter(m.name.ilike('%' + str(q) + '%')).all()
    #    results.extend(r.as_dict() for r in model_results)
    #    if i == 0:
    #        city_name = m.query.filter(m.name.ilike('%' + str(q) + '%')).from_self(City.name)
    #    elif city_name is not None:
    #        result = m.query.filter(m.city_name.ilike(city_name[0])).all()
    #        results.extend(r.as_dict() for r in result)
    #        if i > 1: 
    #            seen = set()
    #            seen.add(r.id for r in result)
    #            result = m.query.filter(m.description.ilike('%' + str(q) + '%')).all()
    #            results.extend(r.as_dict() for r in result if r.id not in seen)
    #results = list(results)
    
    return results
