import requests
import time
import json


def get_image(city):
    url1 = (
        "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input="
        + city
        + "&inputtype=textquery&key=AIzaSyDNI70aeDL4UhstgwyMHO86Yzq4pYO8bB0&sensor=false"
    )
    response = requests.get(url1)
    data = response.json()
    place_id = data["candidates"][0]["place_id"]
    url2 = (
        "https://maps.googleapis.com/maps/api/place/details/json?place_id="
        + place_id
        + "&key=AIzaSyDNI70aeDL4UhstgwyMHO86Yzq4pYO8bB0&sensor=false"
    )
    response2 = requests.get(url2)
    cityData = response2.json()
    text = json.dumps(cityData, indent=4)

    try:
        photos = cityData["result"]["photos"]

    except:
        return None
    for photo in photos:
        photo_reference = photo["photo_reference"]
        url3 = (
            "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="
            + photo_reference
            + "&key=AIzaSyDNI70aeDL4UhstgwyMHO86Yzq4pYO8bB0"
        )
        return url3


def update_all_cities(db, City):
    url = "https://wft-geo-db.p.rapidapi.com/v1/geo/cities/"
    cities = json.loads(open("list_of_cities.json").read())
    s2 = "/nearbyCities"
    querystring = {
        "includeDeleted": "NONE",
        "types": "CITY",
        "radius": "100",
        "limit": "10",
        "minPopulation": "100000",
    }
    headers = {
        "x-rapidapi-host": "wft-geo-db.p.rapidapi.com",
        "x-rapidapi-key": "ee993885d4mshc705c9e8c4663d9p153a0fjsn68c162e4bf7e",
    }

    remove = [
        "id",
        "type",
        "city",
        "country",
        "countryCode",
        "regionCode",
        "latitude",
        "longitude",
    ]

    prefix = "citydata/details"
    for i, city in enumerate(cities):
        time.sleep(1.5)
        response = requests.get(url + cities[city], headers=headers).json()
        print("getting data for", city)
        for i in remove:
            try:
                del response["data"][i]
            except KeyError:
                pass
        del response["data"]["deleted"]
        response = response["data"]
        response["id"] = response["wikiDataId"]
        del response["wikiDataId"]
        response["image"] = get_image(city)
        new = City(**response)
        db.session.add(new)
        print("successfully added", city)
    db.session.commit()
