# Filter
comphrehensive list of what can be filtered

## Events
```
api.after-work.site/events?category=<category parameter>
api.after-work.site/events?genre=<genre parameter>
api.after-work.site/events?city=<city parameter>
api.after-work.site/events?family=<family parameter>
```
## Cities
```
api.after-work.site/cities?maxPopulation=<maxPopulation parameter>
api.after-work.site/cities?minPopulation=<minPopulation parameter>
api.after-work.site/cities?maxElevation=<maxElevation parameter>
api.after-work.site/cities?minElevation=<minElevation parameter>
```

## Meetups
```
api.after-work.site/meetups?category=<category parameter>
api.after-work.site/meetups?city=<city parameter>
api.after-work.site/meetups?maxMembers=<maxMembers parameter>
api.after-work.site/meetups?minMembers=<minMembers parameter>
api.after-work.site/meetups?joinMode=<joinMode parameter>
```