from flask import Flask, send_from_directory, request
from flask_cors import CORS
from database import db
from models import Event, Meetup, City
from search import search
from sqlalchemy import text
import json
import os


# Elastic Beanstalk looks for an 'application' that is callable by default
application = Flask(__name__)
CORS(application)
application.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgres://afterwork:kLh76KZ0owJzmlhiIYuP@afterworkdb.c0vtbw5em6hy.us-east-1.rds.amazonaws.com:5432/postgres"
application.config[
    "SQLALCHEMY_TRACK_MODIFICATIONS"
] = False  # get rid of annoying warning message
db.init_app(application)


@application.route("/events")
def get_all_events():
    # get all the events in the database
    category = request.args.get('category') or '%'
    genre = request.args.get('genre') or '%'
    city = request.args.get('city') or '%'
    family = request.args.get('family') == 'True'
    sort_by = request.args.get('sortBy') or 'city_name'
    order = request.args.get('order') or 'asc'

    # if no argument given, want to return all results
    if request.args.get('family') is None:
        family_flag = True
    else:
        family_flag = False

    all_events_obj = Event.query.filter(
        (Event.category.like(category)),
        (Event.genre.like(genre)),
        (Event.city_name.like(city)),
        ((Event.family == family) | (family_flag))
    ).order_by(
        text(sort_by + " " + order)
    ).all()

    # convert the list of Event objects to a list of dicts
    all_events_dict = [e.as_dict() for e in all_events_obj]
    return json.dumps(all_events_dict)


@application.route("/event/<string:event_id>")
def get_one_event(event_id):
    one_event = Event.query.get_or_404(event_id).as_dict()
    return json.dumps(one_event)


@application.route("/meetups")
def get_all_meetups():

    # print(request.args.keys)

    city = request.args.get('city')
    category = request.args.get('category')
    maxMembers = request.args.get('maxMembers')
    minMembers = request.args.get('minMembers')
    join_mode = request.args.get('join_mode')
    sort_by = request.args.get('sortBy') or "name"
    order = request.args.get('order') or 'asc'

    all_meetups = Meetup.query.filter(
        (Meetup.city_name == city) if city else True,
        (Meetup.category == category) if category else True,
        (Meetup.members >= minMembers) if minMembers else True,
        (Meetup.members <= maxMembers) if maxMembers else True,
        (Meetup.join_mode == join_mode) if join_mode else True,
    ).order_by(
        text(sort_by + " " + order)
    ).all()

    meetups_dict = [m.as_dict() for m in all_meetups]

    return json.dumps(meetups_dict)


@application.route("/meetup/<string:meetup_id>")
def get_meetup(meetup_id):
    one_meetup = Meetup.query.get_or_404(meetup_id).as_dict()
    return json.dumps(one_meetup)


@application.route("/cities")
def get_all_cities():
    # get all the cities in the database
    minPopulation = request.args.get('minPopulation') or 0
    maxPopulation = request.args.get('maxPopulation') or 100000000
    minElevation = request.args.get('minElevation') or 0
    maxElevation = request.args.get('maxElevation') or 100000000
    sort_by = request.args.get('sortBy') or 'name'
    order = request.args.get('order') or 'asc'

    all_cities = City.query.filter(
        (City.population >= minPopulation),
        (City.population <= maxPopulation),
        (City.elevationMeters >= minElevation),
        (City.elevationMeters <= maxElevation)
    ).order_by(
        text(sort_by + " " + order)
    ).all()

    # convert the list of Event objects to a list of dicts
    cities_dict = [c.as_dict() for c in all_cities]
    return json.dumps(cities_dict)


# indexed by capitalized city name, not city id
@application.route("/city/<string:city_id>")
def get_city(city_id):
    one_city = City.query.get_or_404(city_id).as_dict()
    return json.dumps(one_city)


@application.route("/search/<string:query>")
def get_search(query):
    results = search(query)
    return json.dumps(results)


@application.route("/vis/meetups")
def get_vis_meetup():
    #num members compared to city population
    all_meetups = Meetup.query.all()
    members_and_pop = []
    for m in all_meetups:
        this_meetup = m.as_dict()
        city = City.query.get(this_meetup["city_name"])
        if city:
            city = city.as_dict()
            pop = city["population"]
            members_and_pop.append((this_meetup["members"], pop))
    return json.dumps(members_and_pop)

# Serve React App


@application.route("/")
def home():
    return send_from_directory("../frontend/build", "index.html")


@application.route("/static/<path:path>")
def send_static(path):
    return send_from_directory("../frontend/build/static", path)


# Run the application
if __name__ == "__main__":
    # Setting debug to True enables debug output. This line should be
    # removed before deploying a production application.
    application.debug = True
    application.run(host="0.0.0.0")
