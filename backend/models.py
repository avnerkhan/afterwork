from database import db
from sqlalchemy import Table, Column, Integer


class Event(db.Model):
    __tablename = "Event"
    id = db.Column(db.String(20), primary_key=True)
    name = db.Column(db.String(220))
    tickets = db.Column(db.String(400))
    image = db.Column(db.String(1000))
    start_date = db.Column(db.String(21))
    category = db.Column(db.String(15))
    genre = db.Column(db.String(20))
    family = db.Column(db.Boolean())
    city_name = db.Column(db.String(20))

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return "instance of class" + self.name


class Meetup(db.Model):
    __tablename__ = "Meetup"
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(100))
    city_name = db.Column(db.String(35))
    image = db.Column(db.String(75))
    link = db.Column(db.String(110))
    description = db.Column(db.Text())
    status = db.Column(db.String(30))
    members = db.Column(db.Integer())
    category = db.Column(db.String(30))
    join_mode = db.Column(db.String(30))
    organizer_name = db.Column(db.String(35))

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return "instance of class" + self.name


class City(db.Model):
    __tablename__ = "City"
    name = db.Column(db.String(20), primary_key=True)
    id = db.Column(db.String(30))
    region = db.Column(db.String(30))
    elevationMeters = db.Column(db.Integer())
    population = db.Column(db.Integer())
    timezone = db.Column(db.String(30))
    image = db.Column(db.String(315))

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return "instance of class" + self.name
