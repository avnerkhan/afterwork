import requests

# ticketmaster dma ids
city_codes = {
    "austin": 222,
    "denver": 264,
    "seattle": 385,
    "chicago": 249,
    "new york": 345,
    "san francisco": 382,
    "los angeles": 324,
    "las vegas": 319,
    "houston": 300,
    "baltimore": 224,
    "philadelphia": 358,
    "phoenix": 359,
    "san antonio": 380,
    "san diego": 381,
    "dallas": 261,
    "san jose": 382,
    "jacksonville": 306,
    "fort worth": 261,
    "columbus": 259,
    "charlotte": 245,
    "indianapolis": 303,
    "washington": 409,
    "boston": 235,
    "el paso": 269,
    "detroit": 266,
    "nashville": 343,
    "portland": 362,
    "memphis": 332,
    "oklahoma city": 349,
    "louisville": 325,
}


def update_all_events(db, Event):
    # set of ids for all the events scraped
    ids = set()

    for city, code in city_codes.items():
        res = requests.get(
            "https://app.ticketmaster.com/discovery/v2/events.json",
            params={
                "classificationName": "music",
                "dmaId": code,
                "apikey": "WEp3qwIF6kzclWPVwTn31cK7c3b25jIR",
            },
        )

        if not res.status_code:
            print("Ticketmaster api responded with an incorrect status code")

        data = res.json()

        try:
            events = data["_embedded"][
                "events"
            ]  # list of all the events in the response
        except KeyError:
            raise Exception(
                "Ticketmaster didn't return the right response, please try running the script again."
            )
        current_city = city

        for e in events[:8]:  # 8 events for each city\
            # don't add duplicate events
            if e["id"] in ids:
                break
            ids |= {e["id"]}

            event_obj = {}
            # name
            event_obj["name"] = e["name"]
            # id
            event_obj["id"] = e["id"]
            # ticket url
            event_obj["tickets"] = e["url"]
            # image
            if e["images"]:
                img = e["images"][0]["url"]
            else:
                img = None
            event_obj["image"] = img
            # start_date
            try:
                event_obj["start_date"] = e["dates"]["start"]["dateTime"]
            except KeyError:
                event_obj["start_date"] = None
            # category
            try:
                event_obj["category"] = e["classifications"][0]["segment"]["name"]
            except:
                event_obj["category"] = None
            # genre
            try:
                event_obj["genre"] = e["classifications"][0]["genre"]["name"]
            except:
                event_obj["genre"] = None
            # family
            try:
                event_obj["family"] = e["classifications"][0]["family"]
            except:
                event_obj["family"] = None
            # city
            event_obj["city"] = current_city
            # create db object
            this_event = Event(
                id=event_obj["id"],
                name=event_obj["name"],
                tickets=event_obj["tickets"],
                image=event_obj["image"],
                start_date=event_obj["start_date"],
                category=event_obj["category"],
                genre=event_obj["genre"],
                family=event_obj["family"],
                city_name=current_city,
            )
            db.session.add(this_event)

    # commit all of the events added this session
    db.session.commit()
