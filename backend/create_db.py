from models import Event, Meetup, City
from application import application, db
from ticketmaster import update_all_events
from wiki import update_all_cities
from meetup import update_all_meetups

# app context fix
app_ctx = application.app_context()
app_ctx.push()

# reset the database
print("Resetting the database...")
db.session.commit()
db.drop_all()
db.create_all()

# pull from ticketmaster and push to database
print("Updating ticketmaster data...")
update_all_events(db, Event)

# pull from geodb and push to database
print("Updating city data...")
update_all_cities(db, City)

# pull from meetups and push to database
print("Updating meetups data...")
update_all_meetups(db, Meetup)

print("Successfully added all new data to the database!")

app_ctx.pop()
