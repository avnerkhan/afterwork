import json
import os
from models import Meetup

# https://api.meetup.com/find/groups?location=austin&radius=3&order=most_active


def update_all_meetups(db, Meetup):

    script_dir = os.path.dirname(__file__)  # <-- absolute dir the script is in
    rel_path = "city_meetups"  # name of folder that all our json files are in
    abs_file_path = os.path.join(script_dir, rel_path)

    for current_filename in os.listdir(abs_file_path):
        file_location = os.path.join(abs_file_path, current_filename)

        with open(file_location) as json_file:
            data = json.load(json_file)

            for group in data[:9]:  # only get the first 8 groups
                try:
                    img_url = group["key_photo"]["photo_link"]
                except:
                    img_url = None
                g_category = group["category"]["name"]
                o_name = group["organizer"]["name"]

                this_meetup = Meetup(
                    id=group["id"],
                    name=group["name"],
                    city_name=group["city"],
                    image=img_url,
                    link=group["link"],
                    description=group["description"],
                    status=group["status"],
                    members=group["members"],
                    category=g_category,
                    join_mode=group["join_mode"],
                    organizer_name=o_name,
                )
                print("Adding " + str(this_meetup.name) + "...")
                db.session.add(this_meetup)

    db.session.commit()
