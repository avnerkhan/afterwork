import {
  CHANGE_SELECTED_EVENT,
  STORE_EVENT_SEARCH
} from "../types/EventInfoTypes";

export function changeEventInfo(EventInfo) {
  return {
    type: CHANGE_SELECTED_EVENT,
    EventInfo
  };
}

export function storeEventSearch(EventSearch) {
  return {
    type: STORE_EVENT_SEARCH,
    EventSearch
  };
}
