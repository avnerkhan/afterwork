import {
  CHANGE_SELECTED_CITY,
  STORE_CITY_SEARCH
} from "../types/CityInfoTypes";

export function changeCityInfo(CityInfo) {
  return {
    type: CHANGE_SELECTED_CITY,
    CityInfo
  };
}

export function storeCitySearch(CitySearch) {
  return {
    type: STORE_CITY_SEARCH,
    CitySearch
  };
}
