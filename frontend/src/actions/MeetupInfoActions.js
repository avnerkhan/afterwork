import {
  CHANGE_SELECTED_MEETUP,
  STORE_MEETUP_SEARCH
} from "../types/MeetupInfoTypes";

export function changeMeetupInfo(MeetupInfo) {
  return {
    type: CHANGE_SELECTED_MEETUP,
    MeetupInfo
  };
}

export function storeMeetupSearch(MeetupSearch) {
  return {
    type: STORE_MEETUP_SEARCH,
    MeetupSearch
  };
}
