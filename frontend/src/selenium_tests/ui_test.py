from selenium import webdriver

driver = webdriver.Chrome("./chromedriver")

# todo remove pass from these tests

# todo use unittest for these methods

def test_1():
    driver.get("http://localhost:3000/")
    elements = driver.find_elements_by_tag_name("a")
    elements[0].click()
    assert "http://localhost:3000/" == driver.current_url


def test_2():
    driver.get("http://localhost:3000/")
    elements = driver.find_elements_by_tag_name("a")
    elements[1].click()
    assert "http://localhost:3000/About" == driver.current_url


def test_3():
    driver.get("http://localhost:3000/")
    elements = driver.find_elements_by_tag_name("a")
    elements[2].click()
    assert "http://localhost:3000/Cities" == driver.current_url


def test_4():
    driver.get("http://localhost:3000/")
    elements = driver.find_elements_by_tag_name("a")
    elements[3].click()
    assert "http://localhost:3000/Meetups" == driver.current_url
    pass


def test_5():
    driver.get("http://localhost:3000/")
    elements = driver.find_elements_by_tag_name("a")
    elements[4].click()
    assert "http://localhost:3000/Events" == driver.current_url
    pass


def test_6():
    driver.get("http://localhost:3000/")
    elements = driver.find_elements_by_tag_name("a")
    elements[5].click()
    assert "http://localhost:3000/Visualizations" == driver.current_url
    pass


def test_7():
    driver.get("http://localhost:3000/About")
    elements = driver.find_elements_by_tag_name("a")
    elements[0].click()
    assert "https://gitlab.com/avnerkhan/afterwork" == driver.current_url
    pass


def test_8():
    driver.get("http://localhost:3000/About")
    elements = driver.find_elements_by_tag_name("a")
    elements[1].click()
    assert "http://localhost:3000/About" != driver.current_url
    pass

def test_9():
    driver.get("http://localhost:3000/Search")
    elements = driver.find_elements_by_tag_name("a")
    elements[0].click()
    assert "http://localhost:3000/Search" == driver.current_url
    pass

def test_10():
    driver.get("http://localhost:3000/Cities")
    elements = driver.find_elements_by_css_selector(".col > button")
    elements[0].click()
    assert "http://localhost:3000/Cities" == driver.current_url
    pass

test_1()
test_2()
test_3()
test_4()
test_5()
test_6()
test_7()
test_8()
test_9()
test_10()

print("10 tests passed")