import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import SearchResults from "./components/SearchResults";
import HomePage from "./components/HomePage";
import AboutPage from "./components/AboutPage";
import CityPage from "./components/CityPage";
import MeetupPage from "./components/MeetupPage";
import EventPage from "./components/EventPage";
import Visualizations from "./components/Visualization";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter, Route, Link } from "react-router-dom";
import { Navbar, Form, FormControl, Button } from "react-bootstrap";
import { Provider } from "react-redux";
import { createStore, combineReducers } from "redux";
import { CityInfoReducer } from "./reducers/CityInfoReducer";
import { EventInfoReducer } from "./reducers/EventInfoReducer";
import { MeetupInfoReducer } from "./reducers/MeetupInfoReducer";
import OneCityPage from "./components/OneCityPage";
import OneMeetupPage from "./components/OneMeetupPage";
import OneEventPage from "./components/OneEventPage";
import SearchBar from "./components/SearchBar"

const store = createStore(
  combineReducers({
    CityInfo: CityInfoReducer,
    EventInfo: EventInfoReducer,
    MeetupInfo: MeetupInfoReducer
  })
);

const webroutes = (
  <Provider store={store}>
    <BrowserRouter>
      <div>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/About" component={AboutPage} />
        <Route exact path="/Cities" component={CityPage} />
        <Route path="/Cities/:info" component={OneCityPage} />
        <Route exact path="/Meetups" component={MeetupPage} />
        <Route path="/Meetups/:info" component={OneMeetupPage} />
        <Route exact path="/Events" component={EventPage} />
        <Route path="/Events/:info" component={OneEventPage} />
        <Route exact path="/Visualizations" component={Visualizations} />
        <Route exact path="/Search" component={SearchResults} />
        <Navbar fixed="top" bg="primary" variant="dark">
          <Link to="/">
            <Navbar.Brand style={{ color: "black" }}>AfterWork</Navbar.Brand>
          </Link>
          <Link to="/About">
            <Navbar.Brand>About</Navbar.Brand>
          </Link>
          <Link to="/Cities">
            <Navbar.Brand>Cities</Navbar.Brand>
          </Link>
          <Link to="/Meetups">
            <Navbar.Brand>Meetups</Navbar.Brand>
          </Link>
          <Link to="/Events">
            <Navbar.Brand>Events</Navbar.Brand>
          </Link>
          <Link to="/Visualizations">
            <Navbar.Brand>Visualizations</Navbar.Brand>
          </Link>
          <Navbar.Brand>
            <SearchBar/>
          </Navbar.Brand>
        </Navbar>
      </div>
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(webroutes, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
