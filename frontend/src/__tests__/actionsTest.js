import { changeCityInfo } from "../actions/CityInfoActions";
import { changeMeetupInfo } from "../actions/MeetupInfoActions";
import { changeEventInfo } from "../actions/EventInfoActions";
import { CHANGE_SELECTED_MEETUP } from "../types/MeetupInfoTypes";
import { CHANGE_SELECTED_CITY } from "../types/CityInfoTypes";
import { CHANGE_SELECTED_EVENT } from "../types/EventInfoTypes";
import { testEvent, testCity, testMeetup } from "../info/static";

const newCityInfo = testCity;
const newMeetupInfo = testMeetup;
const newEventInfo = testEvent;

describe("Action Tests", () => {
  it("should equal the new CityInfo Action", () => {
    const CityInfo = newCityInfo;
    const expectedType = {
      type: CHANGE_SELECTED_CITY,
      CityInfo
    };
    expect(changeCityInfo(CityInfo)).toEqual(expectedType);
  });

  it("should equal the new MeetupInfo Action", () => {
    const MeetupInfo = newMeetupInfo;
    const expectedType = {
      type: CHANGE_SELECTED_MEETUP,
      MeetupInfo
    };
    expect(changeMeetupInfo(MeetupInfo)).toEqual(expectedType);
  });

  it("should equal the new EventInfo Action", () => {
    const EventInfo = newEventInfo;
    const expectedType = {
      type: CHANGE_SELECTED_EVENT,
      EventInfo
    };
    expect(changeEventInfo(EventInfo)).toEqual(expectedType);
  });
});
