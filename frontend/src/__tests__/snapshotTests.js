import { AboutPage } from "../components/AboutPage";
import renderer from "react-test-renderer";
import { AboutTotals } from "../components/AboutTotals";
import { CityPage } from "../components/CityPage";
import { EventPage } from "../components/EventPage";
import { MeetupPage } from "../components/MeetupPage";
import { HomePage } from "../components/HomePage";
import { OneCityPage } from "../components/OneCityPage";
import { OneEventPage } from "../components/OneEventPage";
import { OneMeetupPage } from "../components/OneMeetupPage";
import { Visualization } from "../components/Visualization";

describe("Component Snapshot Tests", () => {
  it("should match AboutPage snapshot", () => {
    const component = renderer.create(AboutPage);
    let aboutPage = component.toJSON();
    expect(aboutPage).toMatchSnapshot();
  });

  it("should match AboutTotals snapshot", () => {
    const component = renderer.create(AboutTotals);
    let aboutTotals = component.toJSON();
    expect(aboutTotals).toMatchSnapshot();
  });

  it("should match CityPage snapshot", () => {
    const component = renderer.create(CityPage);
    let cityPage = component.toJSON();
    expect(cityPage).toMatchSnapshot();
  });
  it("should match EventPage snapshot", () => {
    const component = renderer.create(EventPage);
    let eventPage = component.toJSON();
    expect(eventPage).toMatchSnapshot();
  });
  it("should match HomePage snapshot", () => {
    const component = renderer.create(HomePage);
    let homePage = component.toJSON();
    expect(homePage).toMatchSnapshot();
  });
  it("should match MeetupPage snapshot", () => {
    const component = renderer.create(MeetupPage);
    let meetupPage = component.toJSON();
    expect(meetupPage).toMatchSnapshot();
  });
  it("should match OneCityPage snapshot", () => {
    const component = renderer.create(OneCityPage);
    let oneCityPage = component.toJSON();
    expect(oneCityPage).toMatchSnapshot();
  });
  it("should match OneEventPage snapshot", () => {
    const component = renderer.create(OneEventPage);
    let oneEventPage = component.toJSON();
    expect(oneEventPage).toMatchSnapshot();
  });
  it("should match OneMeetupPage snapshot", () => {
    const component = renderer.create(OneMeetupPage);
    let oneEventPage = component.toJSON();
    expect(oneEventPage).toMatchSnapshot();
  });
  it("should match Visualization snapshot", () => {
    const component = renderer.create(Visualization);
    let visualization = component.toJSON();
    expect(visualization).toMatchSnapshot();
  });
});
