import { CityInfoReducer } from "../reducers/CityInfoReducer";
import { MeetupInfoReducer } from "../reducers/MeetupInfoReducer";
import { EventInfoReducer } from "../reducers/EventInfoReducer";
import { changeCityInfo } from "../actions/CityInfoActions";
import { changeMeetupInfo } from "../actions/MeetupInfoActions";
import { changeEventInfo } from "../actions/EventInfoActions";
import { testEvent, testCity, testMeetup } from "../info/static";

const newCityInfo = testCity;
const newMeetupInfo = testMeetup;
const newEventInfo = testEvent;

describe("Reducer Tests", () => {
  it("should equal the new CityInfo", () => {
    const returnType = changeCityInfo(newCityInfo);
    const newState = CityInfoReducer({}, returnType);
    expect(newState.CityInfo).toEqual(newCityInfo);
  });

  it("should equal the new MeetupInfo", () => {
    const returnType = changeMeetupInfo(newMeetupInfo);
    const newState = MeetupInfoReducer({}, returnType);
    expect(newState.MeetupInfo).toEqual(newMeetupInfo);
  });

  it("should equal the new EventInfo", () => {
    const returnType = changeEventInfo(newEventInfo);
    const newState = EventInfoReducer({}, returnType);
    expect(newState.EventInfo).toEqual(newEventInfo);
  });
});
