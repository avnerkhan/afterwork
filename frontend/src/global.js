import React from "react";
import { Image, ListGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import { baseServerUrl } from "./info/static";

export function displayTableHeader(tableHeaders) {
  return (
    <thead>
      <tr>
        {tableHeaders.map(header => {
          return <th>{header}</th>;
        })}
      </tr>
    </thead>
  );
}

export function getQueryString(json) {
  let toReturn = "";

  for (const key of Object.keys(json)) {
    if (String(json[key]) !== "%")
      toReturn += key + "=" + String(json[key]) + "&";
  }

  return toReturn;
}

function convertToReadable(date) {
  if (date == undefined) return "No Date Found";
  let stringDate = String(date);
  let newString = "";
  for (const letter of stringDate) {
    if (letter === "T") break;
    newString += letter;
  }
  return newString;
}

function convertToNormal(info) {
  return info ? "Yes" : "No";
}

export async function findCorrectCity(infoString) {
  const res = await fetch(baseServerUrl + "cities");
  const data = await res.json();
  if (infoString.toLowerCase() === "new york") {
    infoString += " city";
  }
  for (const city of data) {
    if (String(city.name).toLowerCase() === infoString.toLowerCase()) {
      return city;
    }
  }
}

function determineTableInfo(info, i, wellOrderingKey, props) {
  if (wellOrderingKey !== "start_date" && wellOrderingKey !== "family") {
    const infoString = String(info[i][wellOrderingKey]);
    return wellOrderingKey === "city_name" ? (
      <Link
        to={"/Cities/" + infoString}
        onClick={async () =>
          props.changeCityInfo(await findCorrectCity(infoString))
        }
      >
        {infoString}
      </Link>
    ) : (
      infoString
    );
  } else {
    return wellOrderingKey === "start_date"
      ? convertToReadable(info[i][wellOrderingKey])
      : convertToNormal(info[i][wellOrderingKey]);
  }
}

function displayTableRows(
  info,
  currIndex,
  isEvents,
  props,
  wellOrderingKeys,
  route,
  indexerKeyName,
  indexerDisplayName
) {
  let rows = [];

  for (
    let i = currIndex * 10;
    i < info.length && i < currIndex * 10 + 10;
    i++
  ) {
    rows.push(
      <tr
        onClick={() =>
          isEvents
            ? props.changeEventInfo(info[i])
            : props.changeMeetupInfo(info[i])
        }
      >
        <td>
          <Image style={{ width: "10rem" }} src={info[i]["image"]} />
        </td>
        {wellOrderingKeys.map(wellOrderingKey => {
          return wellOrderingKey === "name" ? (
            <td>
              <a href={info[i][indexerKeyName]}>
                {determineTableInfo(info, i, wellOrderingKey, props)}
              </a>
            </td>
          ) : (
            <td>{determineTableInfo(info, i, wellOrderingKey, props)}</td>
          );
        })}
        <Link to={route + info[i]["name"]}>More Information</Link>
      </tr>
    );
  }
  return rows;
}

export function displayTableInfo(
  info,
  isEvents,
  props,
  wellOrderingKeys,
  currIndex
) {
  const route = isEvents ? "/Events/" : "/Meetups/";
  const indexerKeyName = isEvents ? "tickets" : "link";
  const indexerDisplayName = isEvents ? "Buy Tickets" : "Link";
  return (
    <tbody>
      {displayTableRows(
        info,
        currIndex,
        isEvents,
        props,
        wellOrderingKeys,
        route,
        indexerKeyName,
        indexerDisplayName
      )}
    </tbody>
  );
}

export function determineNumberOfPagination(info, isTable = true) {
  const div = isTable ? 10 : 9;
  const base = Math.floor(info.length / div);
  return info.length % div !== 0 ? base + 1 : base;
}

export function getTop(data, cityName) {
  let info = [];
  for (const information of data) {
    if (
      String(
        information.city_name.toLowerCase() === "new york"
          ? information.city_name + " city"
          : information.city_name
      ).toLowerCase() === String(cityName).toLowerCase()
    ) {
      info.push(information);
    }
  }
  return info;
}

export function displayTop(toDisplay, isEvents, props) {
  return (
    <ListGroup>
      {toDisplay.map(info => {
        return (
          <Link
            to={(isEvents ? "/events/" : "/meetups/") + info.name}
            onClick={() =>
              isEvents
                ? props.changeEventInfo(info)
                : props.changeMeetupInfo(info)
            }
          >
            <ListGroup.Item style={{ color: "black" }}>
              {info.name + " "}
            </ListGroup.Item>
          </Link>
        );
      })}
    </ListGroup>
  );
}
