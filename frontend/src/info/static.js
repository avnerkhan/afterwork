export const aboutInfo =
  "AfterWork is a revolutionary web application that allows people to find fun meetups and events going on in their city of choosing";

export const baseServerUrl = "https://api.after-work.site/";
export const baseDevURL = "https://api.foodmeonce.me/";
//"http://0.0.0.0:5000/"
//"http://api.after-work.site/";

export const testMeetup = {
  id: 19719014,
  name: "Baltimore History & Culture: Art, Food, Museums, Tours, etc.",
  city_name: "Baltimore",
  image:
    "https://secure.meetupstatic.com/photos/event/4/d/c/e/600_484999918.jpeg",
  link: "https://www.meetup.com/BaltimoreHistoryAndCulture/",
  description: "test",
  status: "active",
  members: 7950,
  category: "Socializing",
  join_mode: "open",
  organizer_name: "Robert Kelleman"
};

export const testCity = {
  name: "Austin",
  id: "Q16559",
  region: "Texas",
  elevationMeters: 149,
  population: 931830,
  timezone: "America__Chicago",
  image:
    "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=CmRaAAAAskPC3tlrmNp11r1FTKKtSG5dWHQ21YlS8pPv97YwwQ6JbgjFbjujs4yB4St84n6fhtSw6KTvtvwsgL4yIT3zGUaWNi_S1sFxV4kMbOKBoEW24j392Cd28-QJR3cxTtiAEhCWcsyFiRUtEyayBa7feo07GhSkSF2y734QmHzBv06T0rmrke65NA&key=AIzaSyDNI70aeDL4UhstgwyMHO86Yzq4pYO8bB0"
};

export const testEvent = {
  id: "G5dIZ4INIHbTZ",
  name: "Bad Bunny",
  tickets:
    "https://www.ticketmaster.com/bad-bunny-san-antonio-texas-11-09-2019/event/3A005688E8F18FC0",
  image:
    "https://s1.ticketm.net/dam/a/3d1/ac6e19d2-2bf7-4035-bb42-4deae968f3d1_1026531_EVENT_DETAIL_PAGE_16_9.jpg",
  start_date: "2019-11-10T02:00:00Z",
  category: "Music",
  genre: "Hip-Hop/Rap",
  family: false,
  city_name: "austin"
};
//"https://api.after-work.site/"
//"http://0.0.0.0:5000/";
