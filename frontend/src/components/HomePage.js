/* eslint-disable */
import React from "react";
import "../css_files/App.css";

export class HomePage extends React.Component {
  render() {
    return (
      <div className="App">
        <div className="App-header-img">
          <div className="center-text-container">
          <h1>Find fun in a city near you</h1>
          <p>
            AfterWork is a web app used to help people find meetups and other
            fun events in any city of their choosing.
          </p>
          </div>
          
        </div>
      </div>
    );
  }
}
export default HomePage;
