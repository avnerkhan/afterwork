import React from "react";
import { Table, Pagination, Button, Row, Col, Form } from "react-bootstrap";
import {
  displayTableHeader,
  displayTableInfo,
  determineNumberOfPagination
} from "../global";
import { connect } from "react-redux";
import { baseServerUrl } from "../info/static";
import { changeMeetupInfo } from "../actions/MeetupInfoActions";
import { getQueryString } from "../global";
import { changeCityInfo } from "../actions/CityInfoActions";
import "../css_files/App.css";

export class MeetupPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHeaders: [
        "Image",
        "Name",
        "City",
        "Status",
        "Event Type",
        "Event Count",
        "Organizer",
        "Join Mode",
        "More Information"
      ],
      wellOrderingKeys: [
        "name",
        "city_name",
        "status",
        "category",
        "members",
        "organizer_name",
        "join_mode"
      ],
      meetups: [],
      currentPagIndex: 0,
      currentSelectedBoxIndex: 0,
      totalPotentialPag: 0,
      selectedOption: "name",
      minMembers: 0,
      maxMembers: 100000,
      category: "%",
      city: "%",
      joinMode: "%",
      showFilter: true,
      order: "asc"
    };
  }

  componentWillMount() {
    fetch(baseServerUrl + "meetups")
      .then(res => res.json())
      .then(data =>
        this.setState({
          meetups: data,
          totalPotentialPag: determineNumberOfPagination(data)
        })
      );
  }

  showNextUntilEnd() {
    let toShow = [];
    let start = this.state.currentPagIndex;
    let end =
      this.state.currentPagIndex + 5 < this.state.totalPotentialPag
        ? this.state.currentPagIndex + 5
        : this.state.totalPotentialPag;

    for (let i = start; i < end; i++) {
      toShow.push(
        <Pagination.Item
          onClick={() => this.setState({ currentSelectedBoxIndex: i })}
        >
          {i + 1}
        </Pagination.Item>
      );
    }

    return toShow;
  }

  displayMeetupPagination() {
    return (
      <Pagination>
        <Pagination.Prev
          disabled={this.state.currentPagIndex === 0}
          onClick={() =>
            this.setState({
              currentPagIndex: this.state.currentPagIndex - 1
            })
          }
        />
        {this.showNextUntilEnd()}
        <Pagination.Next
          disabled={
            this.state.currentPagIndex + 5 > this.state.totalPotentialPag
          }
          onClick={() =>
            this.setState({
              currentPagIndex: this.state.currentPagIndex + 1
            })
          }
        />
      </Pagination>
    );
  }

  sendAndUpdateMeetup() {
    const toSend = {
      minMembers: Number(this.state.minMembers),
      maxMembers: Number(this.state.maxMembers),
      category: String(this.state.category),
      joinMode: String(this.state.joinMode),
      city: String(this.state.city),
      sortBy: String(this.state.selectedOption),
      order: String(this.state.order)
    };

    fetch(baseServerUrl + "meetups?" + getQueryString(toSend))
      .then(res => res.json())
      .then(data =>
        this.setState({
          meetups: data,
          totalPotentialPag: determineNumberOfPagination(data, false)
        })
      );
  }

  displayFilterInfo() {
    return this.state.showFilter ? (
      <Row>
        <Col>
          <div>Order of sort</div>
          <select onChange={e => this.setState({ order: e.target.value })}>
            <option value="asc">Ascending</option>
            <option value="desc">Descending</option>
          </select>
        </Col>
        <Col>
          <div>Sort By</div>
          <select
            onChange={e => this.setState({ selectedOption: e.target.value })}
          >
            <option value="name">Meetup Name</option>
            <option value="members">Event Count</option>
            <option value="category">Event Type</option>
            <option value="city_name">City</option>
          </select>
        </Col>
        <Col>
          <div>Filter</div>
          <Form>
            <div>Members</div>
            <Row>
              <Col>
                <Form.Control
                  placeholder="Min"
                  onChange={e => this.setState({ minMembers: e.target.value })}
                />
              </Col>
              <Col>
                <Form.Control
                  placeholder="Max"
                  onChange={e => this.setState({ maxMembers: e.target.value })}
                />
              </Col>
            </Row>
            <div>Event Type</div>
            <select onChange={e => this.setState({ category: e.target.value })}>
              <option value="%">None</option>
              <option value="Tech">Tech</option>
              <option value="Socializing">Socializing</option>
              <option value="Career & Business">Career & Business</option>
              <option value="Lifestyle">Lifestyle</option>
              <option value="Socializing">Socializing</option>
              <option value="Singles">Singles</option>
              <option value="Food & Drink">Food & Drink</option>
              <option value="Book Clubs">Book Clubs</option>
              <option value="Fitness">Fitness</option>
            </select>
            <div>Join Mode</div>
            <select onChange={e => this.setState({ joinMode: e.target.value })}>
              <option value="%">None</option>
              <option value="open">Open</option>
              <option value="approval">Approval</option>
            </select>
            <div>City</div>
            <select onChange={e => this.setState({ city: e.target.value })}>
              <option value="%">None</option>
              <option value="Austin">Austin</option>
              <option value="New York">New York</option>
              <option value="Baltimore">Baltimore</option>
              <option value="Boston">Boston</option>
            </select>
          </Form>
        </Col>
        <Col>
          <Button
            onClick={() => {
              this.sendAndUpdateMeetup();
            }}
          >
            Apply
          </Button>
        </Col>
      </Row>
    ) : null;
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          {this.displayFilterInfo()}
          <Button
            onClick={() =>
              this.setState({ showFilter: !this.state.showFilter })
            }
          >
            Show Filter Options
          </Button>
          <Table striped hovered bordered>
            {displayTableHeader(this.state.tableHeaders)}
            {displayTableInfo(
              this.state.meetups,
              false,
              this.props,
              this.state.wellOrderingKeys,
              this.state.currentSelectedBoxIndex
            )}
          </Table>
          {this.displayMeetupPagination()}
        </div>
      </div>
    );
  }
}

export const mapStateToProps = state => ({
  meetup: state.MeetupInfo.MeetupInfo
});

const mapDispatchToProps = {
  changeMeetupInfo,
  changeCityInfo
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MeetupPage);
