/* eslint-disable */

import React from "react";
import { connect } from "react-redux";
import "../css_files/App.css";
import { baseServerUrl } from "../info/static";
import { Image } from "react-bootstrap";
import { Link } from "react-router-dom";
import { changeCityInfo } from "../actions/CityInfoActions";
import { changeEventInfo } from "../actions/EventInfoActions";
import { findCorrectCity, displayTop, getTop } from "../global";

export class OneMeetupPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      topEvents: []
    };
  }

  componentWillMount() {
    fetch(baseServerUrl + "events")
      .then(res => res.json())
      .then(data =>
        this.setState({
          topEvents: getTop(data, this.props.meetup.city_name)
        })
      );
  }

  parseDescription(description) {
    if (description != undefined) {
      const regex = /(<([^>]+)>)/gi;
      return description.replace(regex, "");
    }
    return "";
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <Image src={this.props.meetup.image} />
          <h1>{this.props.meetup.name}</h1>
          <Link
            to={"/Cities/" + this.props.meetup.city_name}
            onClick={async () =>
              this.props.changeCityInfo(
                await findCorrectCity(this.props.meetup.city_name)
              )
            }
          >
            <h2> City: {this.props.meetup.city_name} </h2>{" "}
          </Link>
          <h2>Organizer: {this.props.meetup.organizer_name}</h2>
          <h2>Category: {this.props.meetup.category}</h2>
          <h2>
            Description: {this.parseDescription(this.props.meetup.description)}
          </h2>
          <h2>Top Events in this city</h2>
          {displayTop(this.state.topEvents, true, this.props)}
        </div>
      </div>
    );
  }
}

export const mapStateToProps = state => ({
  meetup: state.MeetupInfo.MeetupInfo
});

const mapDispatchToProps = {
  changeCityInfo,
  changeEventInfo
};

export default connect(mapStateToProps, mapDispatchToProps)(OneMeetupPage);
