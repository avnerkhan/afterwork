/* eslint-disable */

import React from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { baseServerUrl } from "../info/static";
import { findCorrectCity, getTop, displayTop } from "../global";
import { changeCityInfo } from "../actions/CityInfoActions";
import { changeMeetupInfo } from "../actions/MeetupInfoActions";
import { Link } from "react-router-dom";
import "../css_files/OneEventPage.css";
import "../css_files/App.css";

export class OneEventPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      topMeetups: []
    };
  }

  componentWillMount() {
    fetch(baseServerUrl + "meetups")
      .then(res => res.json())
      .then(data =>
        this.setState({
          topMeetups: getTop(data, this.props.event.city_name)
        })
      );
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          {" "}
          <Container>
            <h1> {this.props.event.name} </h1>
            {/*Image goes here*/}{" "}
            <img className="event-image" src={this.props.event.image} />{" "}
            <p> Date: {this.props.event.start_date} </p>{" "}
            <Link
              to={"/Cities/" + this.props.event.city_name}
              onClick={async () =>
                this.props.changeCityInfo(
                  await findCorrectCity(this.props.event.city_name)
                )
              }
            >
              <p> City: {this.props.event.city_name} </p>{" "}
            </Link>
            <p>Category: {this.props.event.category} </p>{" "}
            <p>Genre: {this.props.event.genre} </p> {/*TODO: fix family */}{" "}
            <Button variant="primary" href={this.props.event.tickets}>
              Buy Tickets{" "}
            </Button>
            <h2>Top Meetups also in this city</h2>
            {displayTop(this.state.topMeetups, false, this.props)}{" "}
          </Container>{" "}
        </div>
      </div>
    );
  }
}

export const mapStateToProps = state => ({
  event: state.EventInfo.EventInfo
});

const mapDispatchToProps = {
  changeCityInfo,
  changeMeetupInfo
};

export default connect(mapStateToProps, mapDispatchToProps)(OneEventPage);
