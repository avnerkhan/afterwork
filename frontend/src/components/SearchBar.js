/* eslint-disable */

import React from "react";
import { Form, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { storeCitySearch } from "../actions/CityInfoActions";
import { storeEventSearch } from "../actions/EventInfoActions";
import { storeMeetupSearch } from "../actions/MeetupInfoActions";
import { baseServerUrl } from "../info/static";
import { Route, Link } from "react-router-dom";

export class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ""
    };
  }

  findProperPlaces(data) {
    this.props.storeCitySearch(data[0]);
    this.props.storeEventSearch(data[1]);
    this.props.storeMeetupSearch(data[2]);
  }

  placeSearchInReducer() {
    fetch(baseServerUrl + "search/" + this.state.value)
      .then(res => res.json())
      .then(data => this.findProperPlaces(data));
  }

  render() {
    return (
      <div>
        <Form inline>
          <Form.Control
            placeholder="Search Website"
            className="mr-sm-2"
            onChange={e => {
              this.setState({ value: e.target.value });
            }}
          />
          <Link to="/Search">
            <Button onClick={() => this.placeSearchInReducer()}>Search</Button>
          </Link>
        </Form>
      </div>
    );
  }
}

export const mapStateToProps = state => ({});

const mapDispatchToProps = {
  storeCitySearch,
  storeMeetupSearch,
  storeEventSearch
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchBar);
