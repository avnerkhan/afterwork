/* eslint-disable */

import React from "react";
import { Image, Card } from "react-bootstrap";
import "../css_files/App.css";
import "../css_files/TechUsed.css";

export class TechUsed extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Card>
        <Image
          className="image-center"
          src={this.props.image}
          style={{ width: "20rem" }}
        />
        <div className="card-text">
          <h1>{this.props.name}</h1>
          <p>{this.props.description}</p>
        </div>
      </Card>
    );
  }
}

export default TechUsed;
