/* eslint-disable */

import React from "react";
import {
  RadialChart,
  XYPlot,
  XAxis,
  YAxis,
  VerticalGridLines,
  HorizontalGridLines,
  VerticalBarSeries,
  VerticalBarSeriesCanvas,
  LabelSeries,
  MarkSeries,
  ChartLabel
} from "react-vis";
import { baseServerUrl } from "../info/static";
import { baseDevURL } from "../info/static";
import "../css_files/App.css";

export class Visualization extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cities: [],
      events: [],
      meetups: [],
      districts: [],
      representatives: [],
      legislations: []
    };
  }

  componentWillMount() {
    fetch(baseServerUrl + "cities")
      .then(res => res.json())
      .then(data =>
        this.setState({
          cities: data
        })
      );
    fetch(baseServerUrl + "meetups")
      .then(res => res.json())
      .then(data =>
        this.setState({
          meetups: data
        })
      );
    fetch(baseServerUrl + "events")
      .then(res => res.json())
      .then(data =>
        this.setState({
          events: data
        })
      );

    fetch(baseDevURL + "Districts?limit=500")
      .then(res => res.json())
      .then(data =>
        this.setState({
          districts: data.data
        })
      );

    fetch(baseDevURL + "Representatives?limit=500")
      .then(res => res.json())
      .then(data =>
        this.setState({
          representatives: data.data
        })
      );

    fetch(baseDevURL + "Legislations?limit=500")
      .then(res => res.json())
      .then(data =>
        this.setState({
          legislations: data.data
        })
      );
  }

  getLegislationsPerStateArr() {
    let states = {};

    console.log(this.state.legislations);

    for (const legislation of this.state.legislations) {
      if (legislation["sponsor_state"] in states) {
        states[legislation["sponsor_state"]] =
          states[legislation["sponsor_state"]] + 1;
      } else {
        states[legislation["sponsor_state"]] = 1;
      }
    }

    return states;
  }

  getDistrictsPerStateArr() {
    let states = {};

    console.log(this.state.districts);

    for (let district of this.state.districts) {
      if (district["state_abbreviation"] in states) {
        states[district["state_abbreviation"]] =
          states[district["state_abbreviation"]] + 1;
      } else {
        states[district["state_abbreviation"]] = 1;
      }
    }
    return states;
  }

  getRepresentativesPerStateArr() {
    let stateToReps = {};

    for (let rep of this.state.representatives) {
      if (rep["state"] in stateToReps) {
        stateToReps[rep["state"]] = stateToReps[rep["state"]] + 1;
      } else {
        stateToReps[rep["state"]] = 1;
      }
    }
    return stateToReps;
  }

  getPercentageOfRepublicanPerState() {
    let totalRepsPerState = this.getRepresentativesPerStateArr();

    let stateToPercentage = {};

    for (let rep of this.state.representatives) {
      if (rep["state"] in stateToPercentage) {
        if (rep["party"] == "Republican") {
          stateToPercentage[rep["state"]] = stateToPercentage[rep["state"]] + 1;
        }
      } else {
        if (rep["party"] == "Republican") {
          stateToPercentage[rep["state"]] = 1;
        }
      }
    }
    console.log(stateToPercentage);

    for (let state in stateToPercentage) {
      stateToPercentage[state] =
        stateToPercentage[state] / totalRepsPerState[state];
    }

    return stateToPercentage;
  }

  getStateVSPerOfRepub() {
    let toReturn = [];
    let stateToPercentage = this.getPercentageOfRepublicanPerState();

    for (let state in stateToPercentage) {
      toReturn.push({
        x: state.toLowerCase(),
        y: stateToPercentage[state]
      });
    }

    console.log(stateToPercentage);
    return toReturn;
  }

  getDistrictsVsLegislation() {
    let toReturn = [];
    let stateToDistricts = this.getDistrictsPerStateArr();
    let stateToLegislation = this.getLegislationsPerStateArr();

    for (let state in stateToDistricts) {
      let numDistricts = stateToDistricts[state];
      let numLegislation = stateToLegislation[state];
      if (numLegislation != undefined) {
        toReturn.push({ x: numLegislation, y: numDistricts });
      }
    }
    return toReturn;
  }

  getPopulations() {
    let toReturn = [];

    for (const city of this.state.cities) {
      toReturn.push({
        subLabel: String(city["name"] + ":" + city["population"]),
        angle: parseInt(city["population"])
      });
    }

    return toReturn;
  }

  getDistrictPopulations() {
    let toReturn = [];

    console.log(this.state.districts);

    for (const district of this.state.districts) {
      toReturn.push({
        subLabel: String(
          district["state"] +
            " district: " +
            district["congressional_district"] +
            ":" +
            district["population"]
        ),
        angle: parseInt(district["population"])
      });
    }

    return toReturn;
  }

  getDistrictsPerState() {
    let toReturn = [];
    let states = {};

    console.log(this.state.districts);

    for (const district of this.state.districts) {
      if (district["state_abbreviation"] in states) {
        states[district["state_abbreviation"]] =
          states[district["state_abbreviation"]] + 1;
      } else {
        states[district["state_abbreviation"]] = 1;
      }
    }

    for (var state in states) {
      toReturn.push({
        subLabel: String(state + " # Districts: " + states[state]),
        angle: parseInt(states[state])
      });
    }

    console.log(states);
    return toReturn;
  }

  getEventGenres() {
    let toReturn = [];
    var dict = {};
    for (const city of this.state.events) {
      dict[city["genre"]] = 0;
    }

    for (const city of this.state.events) {
      dict[city["genre"]] = dict[city["genre"]] + 1;
    }

    for (var key in dict) {
      toReturn.push({
        subLabel: String(key + ":" + dict[key]),
        angle: parseInt(dict[key])
      });
    }

    return toReturn;
  }

  getEventCategories() {
    let toReturn = [];
    var dict = {};
    for (const city of this.state.events) {
      dict[city["category"]] = 0;
    }

    for (const city of this.state.events) {
      dict[city["category"]] = dict[city["category"]] + 1;
    }

    for (var key in dict) {
      toReturn.push({
        subLabel: String(key + ":" + dict[key]),
        angle: parseInt(dict[key])
      });
    }

    return toReturn;
  }

  getCityNames() {
    let toReturn = [];

    for (const city of this.state.cities) {
      toReturn.push({
        name: String(city["name"])
      });
    }

    return toReturn;
  }

  getPopulationInfo() {
    let toReturn = [];

    for (const city of this.state.cities) {
      toReturn.push({
        x: city["name"].toLowerCase(),
        y: parseInt(city["population"])
      });
    }

    return toReturn;
  }

  getTotalAmountOfMeetups(city) {
    let amount = 0;

    for (const meetup of this.state.meetups) {
      if (meetup["city_name"].toLowerCase() === city.toLowerCase()) {
        amount += parseInt(meetup["members"]);
      }
    }

    return amount;
  }

  getMeetupInfo() {
    let toReturn = [];

    for (const city of this.state.cities) {
      toReturn.push({
        x: city["name"].toLowerCase(),
        y: parseInt(this.getTotalAmountOfMeetups(city["name"]))
      });
    }

    return toReturn;
  }

  getMeetupAndPopInfo() {
    // fetch(baseServerUrl + "vis/meetups")
    //   .then(res => res.json())
    //   .then(data => {
    //     return data }
    //   );
    // Hardcoded for now
    return [
      [4228, 931830],
      [1341, 931830],
      [601, 829718],
      [13589, 931830],
      [452, 931830],
      [5237, 931830],
      [1637, 931830],
      [1934, 931830],
      [2180, 931830],
      [5740, 931830],
      [7035, 621849],
      [8975, 621849],
      [7950, 621849],
      [9508, 621849],
      [5817, 621849],
      [5843, 621849],
      [4936, 621849],
      [789, 621849],
      [7249, 621849],
      [8311, 667137],
      [5091, 667137],
      [14324, 667137],
      [7039, 667137],
      [11603, 667137],
      [4082, 667137],
      [3564, 667137],
      [12724, 667137],
      [2210, 827097],
      [886, 827097],
      [3481, 827097],
      [2724, 827097],
      [2702, 827097],
      [6410, 827097],
      [489, 827097],
      [653, 827097],
      [6050, 827097],
      [2220, 2720546],
      [5170, 2720546],
      [3156, 2720546],
      [8630, 2720546],
      [5450, 2720546],
      [7409, 2720546],
      [2906, 2720546],
      [5232, 2720546],
      [4010, 2720546],
      [387, 850106],
      [1328, 850106],
      [1405, 850106],
      [2001, 850106],
      [77, 850106],
      [969, 850106],
      [561, 850106],
      [826, 850106],
      [613, 682545],
      [317, 850106],
      [3467, 1300092],
      [2751, 1300092],
      [5377, 1300092],
      [3562, 1300092],
      [2782, 1300092],
      [440, 655770],
      [3937, 1300092],
      [7121, 1300092],
      [4120, 1300092],
      [6958, 1300092],
      [11928, 682545],
      [3974, 682545],
      [11848, 682545],
      [5082, 682545],
      [3558, 682545],
      [2407, 682545],
      [2797, 682545],
      [6496, 682545],
      [2233, 677116],
      [5493, 677116],
      [3557, 677116],
      [171, 677116],
      [633, 677116],
      [601, 677116],
      [788, 677116],
      [1387, 2296224],
      [2062, 677116],
      [245, 677116],
      [1198, 681124],
      [1968, 681124],
      [278, 681124],
      [171, 681124],
      [119, 681124],
      [616, 681124],
      [1155, 681124],
      [66, 681124],
      [346, 681124],
      [641, 833319],
      [5803, 833319],
      [468, 833319],
      [1258, 833319],
      [1288, 833319],
      [294, 833319],
      [713, 833319],
      [908, 833319],
      [202, 833319],
      [558, 2296224],
      [3829, 2296224],
      [1416, 2296224],
      [1803, 2296224],
      [3125, 2296224],
      [1533, 2296224],
      [1101, 2296224],
      [210, 2296224],
      [5071, 829718],
      [576, 829718],
      [388, 829718],
      [4178, 829718],
      [1092, 829718],
      [3662, 829718],
      [865, 829718],
      [818, 829718],
      [1371, 868031],
      [571, 868031],
      [832, 868031],
      [545, 868031],
      [230, 868031],
      [41, 868031],
      [392, 868031],
      [146, 868031],
      [294, 868031],
      [983, 623747],
      [14386, 623747],
      [3957, 623747],
      [2282, 623747],
      [5520, 623747],
      [10795, 623747],
      [1410, 623747],
      [1391, 623747],
      [379, 623747],
      [5276, 3971883],
      [3521, 3971883],
      [4284, 3971883],
      [9030, 3971883],
      [2077, 3971883],
      [1724, 3971883],
      [1478, 243639],
      [1370, 3971883],
      [1396, 3971883],
      [1808, 3971883],
      [4225, 243639],
      [538, 243639],
      [1913, 243639],
      [904, 243639],
      [365, 243639],
      [69, 243639],
      [817, 243639],
      [985, 243639],
      [2531, 655770],
      [2715, 655770],
      [530, 655770],
      [755, 655770],
      [1194, 655770],
      [178, 655770],
      [888, 530852],
      [194, 655770],
      [427, 655770],
      [3995, 530852],
      [2232, 530852],
      [6024, 530852],
      [2699, 530852],
      [3344, 530852],
      [1802, 530852],
      [2154, 530852],
      [1056, 530852],
      [5019, 631346],
      [2997, 631346],
      [2819, 631346],
      [203, 631346],
      [1543, 631346],
      [170, 631346],
      [885, 631346],
      [1008, 631346],
      [70, 631346],
      [784, 1567442],
      [1936, 1567442],
      [732, 1567442],
      [454, 1567442],
      [538, 1567442],
      [3348, 1567442],
      [821, 1567442],
      [1609, 1567442],
      [6974, 1563025],
      [8396, 1563025],
      [2813, 1563025],
      [5138, 1563025],
      [809, 1563025],
      [519, 1563025],
      [1739, 1469845],
      [590, 1563025],
      [2062, 1563025],
      [4490, 1563025],
      [30509, 632309],
      [5754, 632309],
      [2015, 632309],
      [3822, 864816],
      [2356, 632309],
      [3659, 632309],
      [2741, 632309],
      [3281, 632309],
      [1078, 632309],
      [3183, 632309],
      [496, 1469845],
      [4024, 1026908],
      [134, 1469845],
      [1032, 1469845],
      [1436, 1469845],
      [1293, 1469845],
      [534, 1469845],
      [3056, 1469845],
      [278, 1469845],
      [5305, 1026908],
      [9390, 1394928],
      [6962, 1394928],
      [18228, 1394928],
      [5162, 1394928],
      [2202, 1394928],
      [4401, 1394928],
      [2088, 1394928],
      [32026, 684451],
      [13697, 1394928],
      [2871, 1394928],
      [8086, 864816],
      [8373, 864816],
      [12640, 864816],
      [9325, 864816],
      [2480, 864816],
      [8479, 864816],
      [15547, 864816],
      [18422, 864816],
      [627, 1026908],
      [2761, 1026908],
      [819, 1026908],
      [1118, 1026908],
      [1398, 1026908],
      [902, 1026908],
      [2979, 1026908],
      [8384, 684451],
      [8496, 684451],
      [16942, 684451],
      [11782, 684451],
      [3935, 684451],
      [7090, 684451],
      [2328, 684451],
      [3103, 684451]
    ];
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h1></h1>
          <h1></h1>
          <h1>Our Visualizations</h1>
          <h2>Cities by population</h2>
          <h2></h2>
          <RadialChart
            data={this.getPopulations()}
            width={1000}
            height={1000}
            showLabels={true}
          />
          <br />
          <h2>Meetups vs Population per city</h2>
          <h2></h2>
          <XYPlot
            xType="ordinal"
            style={{ fontSize: "8px" }}
            width={1300}
            height={500}
            xDistance={100}
            stackBy="y"
            margin={{ left: 150 }}
          >
            <VerticalGridLines />
            <HorizontalGridLines />
            <XAxis />
            <YAxis />
            <VerticalBarSeries data={this.getPopulationInfo()} />
            <VerticalBarSeries data={this.getMeetupInfo()} />
          </XYPlot>
          <h2>Meetups per Genre</h2>
          <RadialChart
            data={this.getEventGenres()}
            width={1000}
            height={1000}
            showLabels={true}
          />
          <h1>Developer Visualizations</h1>
          <h2>Districts by State</h2>
          <RadialChart
            data={this.getDistrictsPerState()}
            width={1000}
            height={1000}
            showLabels={true}
          />
          <h2>Number of Districts vs Number of Bills passed</h2>
          <XYPlot
            xType="ordinal"
            style={{ fontSize: "6px" }}
            width={1300}
            height={500}
            xDistance={100}
            stackBy="y"
            margin={{ left: 150 }}
          >
            <VerticalGridLines />
            <HorizontalGridLines />
            <XAxis />
            <YAxis />
            <ChartLabel
              text="Number of Bills"
              className="alt-x-label"
              includeMargin={true}
              xPercent={0.6}
              yPercent={0.7}
              style={{
                fontSize: "14px"
              }}
            />

            <ChartLabel
              text="Number of Districts"
              className="alt-y-label"
              includeMargin={true}
              xPercent={0.1}
              yPercent={0.3}
              style={{
                transform: "rotate(-90)",
                textAnchor: "end",
                fontSize: "14px"
              }}
            />
            <MarkSeries data={this.getDistrictsVsLegislation()} />
            {/* <LabelSeries data={this.getDistrictsVsLegislation()} /> */}
          </XYPlot>

          <h2>State VS Percentage of Republican Representatives</h2>
          <h2></h2>
          <XYPlot
            xType="ordinal"
            style={{ fontSize: "6px" }}
            width={1300}
            height={500}
            xDistance={100}
            stackBy="y"
            margin={{ left: 150 }}
          >
            <VerticalGridLines />
            <HorizontalGridLines />
            <XAxis />
            <YAxis />
            <ChartLabel
              text="State"
              className="alt-x-label"
              includeMargin={true}
              xPercent={0.6}
              yPercent={0.7}
              style={{
                fontSize: "14px"
              }}
            />

            <ChartLabel
              text="Percent Republican"
              className="alt-y-label"
              includeMargin={true}
              xPercent={0.1}
              yPercent={0.3}
              style={{
                transform: "rotate(-90)",
                textAnchor: "end",
                fontSize: "14px"
              }}
            />
            <MarkSeries data={this.getStateVSPerOfRepub()} />
          </XYPlot>
          {/* <h1>Meetups per Category</h1>
        
          <RadialChart
            data={this.getEventCategories()}
            
            width={1000}
            height={1000}
            showLabels={true}
          /> */}
        </div>
      </div>
    );
  }
}

export default Visualization;
