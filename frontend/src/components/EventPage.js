import React from "react";
import { Table, Pagination, Button, Row, Col, Form } from "react-bootstrap";
import {
  displayTableHeader,
  displayTableInfo,
  determineNumberOfPagination
} from "../global";
import { connect } from "react-redux";
import { changeEventInfo } from "../actions/EventInfoActions";
import { changeCityInfo } from "../actions/CityInfoActions";
import { getQueryString } from "../global";
import { baseServerUrl } from "../info/static";
import "../css_files/App.css";

export class EventPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHeaders: [
        "Image",
        "Name",
        "City",
        "Category",
        "Genre",
        "Start Date",
        "Family Friendly",
        "More Information"
      ],
      wellOrderingKeys: [
        "name",
        "city_name",
        "category",
        "genre",
        "start_date",
        "family"
      ],
      events: [],
      currentPagIndex: 0,
      currentSelectedBoxIndex: 0,
      totalPotentialPag: 0,
      selectedOption: "name",
      city: "%",
      genre: "%",
      category: "%",
      showFilter: true,
      order: "asc"
    };
  }

  componentWillMount() {
    fetch(baseServerUrl + "events")
      .then(res => res.json())
      .then(data =>
        this.setState({
          events: data,
          totalPotentialPag: determineNumberOfPagination(data)
        })
      );
  }

  showNextUntilEnd() {
    let toShow = [];
    let start = this.state.currentPagIndex;
    let end =
      this.state.currentPagIndex + 5 < this.state.totalPotentialPag
        ? this.state.currentPagIndex + 5
        : this.state.totalPotentialPag;

    for (let i = start; i < end; i++) {
      toShow.push(
        <Pagination.Item
          onClick={() => this.setState({ currentSelectedBoxIndex: i })}
        >
          {i + 1}
        </Pagination.Item>
      );
    }

    return toShow;
  }

  displayEventPagination() {
    return (
      <Pagination>
        <Pagination.Prev
          disabled={this.state.currentPagIndex === 0}
          onClick={() =>
            this.setState({
              currentPagIndex: this.state.currentPagIndex - 1
            })
          }
        />
        {this.showNextUntilEnd()}
        <Pagination.Next
          disabled={
            this.state.currentPagIndex + 5 > this.state.totalPotentialPag
          }
          onClick={() =>
            this.setState({
              currentPagIndex: this.state.currentPagIndex + 1
            })
          }
        />
      </Pagination>
    );
  }

  displayFilterInfo() {
    return this.state.showFilter ? (
      <Row>
        <Col>
          <div>Order of sort</div>
          <select onChange={e => this.setState({ order: e.target.value })}>
            <option value="asc">Ascending</option>
            <option value="desc">Descending</option>
          </select>
        </Col>
        <Col>
          <div>Sort By</div>
          <select
            onChange={e => this.setState({ selectedOption: e.target.value })}
          >
            <option value="name">Event Name</option>
            <option value="category">Category</option>
            <option value="genre">Genre</option>
            <option value="city_name">City</option>
          </select>
        </Col>
        <Col>
          <div>Filter</div>
          <Form>
            <div>
              <div>Category</div>
              <select
                onChange={e => this.setState({ category: e.target.value })}
              >
                <option value="%">None</option>
                <option value="Music">Music</option>
                <option value="Film">Film</option>
                <option value="Sports">Sports</option>
              </select>
              <div>Genre</div>
              <select onChange={e => this.setState({ genre: e.target.value })}>
                <option value="%">None</option>
                <option value="Motorsports/Racing">Motorsports/Racing</option>
                <option value="Rock">Rock</option>
                <option value="Latin">Latin</option>
                <option value="Hip-Hop/Rap">Hip-Hop/Rap </option>
                <option value="Music">Music</option>
                <option value="Pop">Pop</option>
                <option value="Religious">Religious</option>
                <option value="Dance/Electronic">Dance/Electronic</option>
              </select>
              <div>City</div>
              <select onChange={e => this.setState({ city: e.target.value })}>
                <option value="%">None</option>
                <option value="austin">Austin</option>
                <option value="new york">New York</option>
                <option value="baltimore">Baltimore</option>
                <option value="boston">Boston</option>
              </select>
            </div>
          </Form>
        </Col>
        <Col>
          <Button
            onClick={() => {
              this.sendAndUpdateEvent();
            }}
          >
            Apply
          </Button>
        </Col>
      </Row>
    ) : null;
  }

  sendAndUpdateEvent() {
    const toSend = {
      category: String(this.state.category),
      genre: String(this.state.genre),
      city: String(this.state.city),
      sortBy: String(this.state.selectedOption),
      order: String(this.state.order)
    };

    fetch(baseServerUrl + "events?" + getQueryString(toSend))
      .then(res => res.json())
      .then(data =>
        this.setState({
          events: data,
          totalPotentialPag: determineNumberOfPagination(data, false)
        })
      );
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          {this.displayFilterInfo()}
          <Button
            onClick={() =>
              this.setState({ showFilter: !this.state.showFilter })
            }
          >
            Show Filter Options
          </Button>
          <Table striped bordered hover>
            {displayTableHeader(this.state.tableHeaders)}
            {displayTableInfo(
              this.state.events,
              true,
              this.props,
              this.state.wellOrderingKeys,
              this.state.currentSelectedBoxIndex
            )}
          </Table>
          {this.displayEventPagination()}
        </div>
      </div>
    );
  }
}

export const mapStateToProps = state => ({
  event: state.EventInfo.EventInfo
});

const mapDispatchToProps = {
  changeEventInfo,
  changeCityInfo
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EventPage);
