/* eslint-disable */

import React from "react";
import { Tabs, Tab, ListGroup } from "react-bootstrap";
import { changeCityInfo } from "../actions/CityInfoActions";
import { changeEventInfo } from "../actions/EventInfoActions";
import { changeMeetupInfo } from "../actions/MeetupInfoActions";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

export class SearchResults extends React.Component {
  constructor(props) {
    super(props);
  }

  determineRoute(info) {
    if (info["family"] != undefined) return "/Events";
    else if (info["members"] != undefined) return "/Meetups";
    return "/Cities";
  }

  determineChangeInfo(info) {
    if (info["family"] != undefined) this.props.changeEventInfo(info);
    else if (info["members"] != undefined) this.props.changeMeetupInfo(info);
    else this.props.changeCityInfo(info);
  }

  displaySearchResults(infoToShow) {
    return (
      <ListGroup variant="flush">
        {infoToShow.map(info => {
          return (
            <Link
              to={this.determineRoute(info) + "/" + info["name"]}
              onClick={() => this.determineChangeInfo(info)}
            >
              <ListGroup.Item style={{ color: "black" }}>
                {info["name"]}
              </ListGroup.Item>
            </Link>
          );
        })}
      </ListGroup>
    );
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <Tabs defaultActiveKey="all">
            <Tab eventKey="all" title="All">
              {this.displaySearchResults(
                this.props.CitySearch.concat(
                  this.props.MeetupSearch.concat(this.props.EventSearch)
                )
              )}
            </Tab>
            <Tab eventKey="cities" title="Cities">
              {this.displaySearchResults(this.props.CitySearch)}
            </Tab>
            <Tab eventKey="meetups" title="Meetups">
              {this.displaySearchResults(this.props.MeetupSearch)}
            </Tab>
            <Tab eventKey="events" title="Events">
              {this.displaySearchResults(this.props.EventSearch)}
            </Tab>
          </Tabs>
        </div>
      </div>
    );
  }
}

export const mapStateToProps = state => ({
  CitySearch: state.CityInfo.CitySearch,
  MeetupSearch: state.MeetupInfo.MeetupSearch,
  EventSearch: state.EventInfo.EventSearch
});

const mapDispatchToProps = {
  changeMeetupInfo,
  changeCityInfo,
  changeEventInfo
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchResults);
