/* eslint-disable */

import React from "react";
import { Button, Form, Card, Row, Col, Pagination } from "react-bootstrap";
import "../css_files/App.css";
import "../css_files/CityPage.css";
import { baseServerUrl } from "../info/static";
import { connect } from "react-redux";
import { determineNumberOfPagination } from "../global";
import { Link } from "react-router-dom";
import { getQueryString } from "../global";
import { changeCityInfo } from "../actions/CityInfoActions";
import { number } from "prop-types";

export class CityPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cities: [],
      currentPagIndex: 0,
      currentSelectedBoxIndex: 0,
      totalPotentialPag: 0,
      showFilter: true,
      selectedOption: "population",
      minPopulation: 0,
      maxPopulation: 100000000,
      minElevation: 0,
      maxElevation: 100000000,
      order: "asc"
    };
  }

  componentWillMount() {
    fetch(baseServerUrl + "cities")
      .then(res => res.json())
      .then(data =>
        this.setState({
          cities: data,
          totalPotentialPag: determineNumberOfPagination(data, false)
        })
      );
  }

  showCityCard(city) {
    return (
      <Card style={{ width: "20rem", color: "black" }}>
        <Card.Img
          variant="top"
          src={city["image"]}
          style={{ width: "20rem" }}
        />
        <Card.Header>{city["name"]}</Card.Header>
        <Card.Text>State: {city["region"]}</Card.Text>
        <Card.Text>Population: {String(city["population"])}</Card.Text>
        <Card.Text>Elevation: {String(city["elevationMeters"])}</Card.Text>
        <Card.Text>Timezone: {String(city["timezone"])}</Card.Text>
        <Link to={"/Cities/" + city.name}>
          <Button
            variant="primary"
            onClick={() => this.props.changeCityInfo(city)}
          >
            More info
          </Button>
        </Link>
      </Card>
    );
  }

  displayCityInformationCard() {
    let outerRows = [];
    const rowLimit = 3;
    for (
      let i = this.state.currentSelectedBoxIndex * 9;
      i < this.state.currentSelectedBoxIndex * 9 + 9 &&
      this.state.cities.length;
      i += rowLimit
    ) {
      let innerCols = [];
      for (let c = i; c < i + rowLimit && c < this.state.cities.length; c++) {
        innerCols.push(<Col>{this.showCityCard(this.state.cities[c])}</Col>);
      }
      outerRows.push(<Row>{innerCols}</Row>);
    }
    return outerRows;
  }

  showNextUntilEnd() {
    let toShow = [];
    let start = this.state.currentPagIndex;
    let end =
      this.state.currentPagIndex + 5 < this.state.totalPotentialPag
        ? this.state.currentPagIndex + 5
        : this.state.totalPotentialPag;

    for (let i = start; i < end; i++) {
      toShow.push(
        <Pagination.Item
          onClick={() => this.setState({ currentSelectedBoxIndex: i })}
        >
          {i + 1}
        </Pagination.Item>
      );
    }

    return toShow;
  }

  displayCityPagination() {
    return (
      <Pagination>
        <Pagination.Prev
          disabled={this.state.currentPagIndex == 0}
          onClick={() =>
            this.setState({
              currentPagIndex: this.state.currentPagIndex - 1
            })
          }
        />
        {this.showNextUntilEnd()}
        <Pagination.Next
          disabled={
            this.state.currentPagIndex + 5 > this.state.totalPotentialPag
          }
          onClick={() =>
            this.setState({
              currentPagIndex: this.state.currentPagIndex + 1
            })
          }
        />
      </Pagination>
    );
  }

  sendAndUpdateCity() {
    const toSend = {
      minElevation: Number(this.state.minElevation),
      maxElevation: Number(this.state.maxElevation),
      minPopulation: Number(this.state.minPopulation),
      maxPopulation: Number(this.state.maxPopulation),
      sortBy: String(this.state.selectedOption),
      order: String(this.state.order)
    };

    fetch(baseServerUrl + "cities?" + getQueryString(toSend))
      .then(res => res.json())
      .then(data =>
        this.setState({
          cities: data,
          totalPotentialPag: determineNumberOfPagination(data, false)
        })
      );
  }

  displayFilterInfo() {
    return this.state.showFilter ? (
      <Row>
        <Col>
          <div>Order of sort</div>
          <select onChange={e => this.setState({ order: e.target.value })}>
            <option value="asc">Ascending</option>
            <option value="desc">Descending</option>
          </select>
        </Col>
        <Col>
          <div>Sort By</div>
          <select
            onChange={e => this.setState({ selectedOption: e.target.value })}
          >
            <option value="population">Population</option>
            <option value="name">City Name</option>
            <option value="region">State</option>
            <option value="timezone">Timezone</option>
          </select>
        </Col>
        <Col>
          <div>Filter</div>
          <Form>
            <div>Population</div>
            <Row>
              <Col>
                <Form.Control
                  placeholder="Min"
                  onChange={e =>
                    this.setState({ minPopulation: e.target.value })
                  }
                />
              </Col>
              <Col>
                <Form.Control
                  placeholder="Max"
                  onChange={e =>
                    this.setState({ maxPopulation: e.target.value })
                  }
                />
              </Col>
            </Row>
            <div>Elevation</div>
            <Row>
              <Col>
                <Form.Control
                  placeholder="Min"
                  onChange={e =>
                    this.setState({ minElevation: e.target.value })
                  }
                />
              </Col>
              <Col>
                <Form.Control
                  placeholder="Max"
                  onChange={e =>
                    this.setState({ maxElevation: e.target.value })
                  }
                />
              </Col>
            </Row>
          </Form>
        </Col>
        <Col>
          <Button
            onClick={() => {
              this.sendAndUpdateCity();
            }}
          >
            Apply
          </Button>
        </Col>
      </Row>
    ) : null;
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          {this.displayFilterInfo()}
          <Button
            onClick={() =>
              this.setState({ showFilter: !this.state.showFilter })
            }
          >
            Show Filter Options
          </Button>
          {this.displayCityInformationCard()}
          {this.displayCityPagination()}
        </div>
      </div>
    );
  }
}

export const mapStateToProps = state => ({
  city: state.CityInfo.CityInfo
});

const mapDispatchToProps = {
  changeCityInfo
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CityPage);
