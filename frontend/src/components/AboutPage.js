/* eslint-disable */

import React from "react";
import AboutTotals from "./AboutTotals.js";
import TechUsed from "./TechUsed";
import "../css_files/App.css";
import { Card, Col, Row } from "react-bootstrap";
import Avner from "../info/images/avner.jpg";
import Alvaro from "../info/images/alvarohareimage.JPG";
import Kyle from "../info/images/kyle.jpeg";
import Aryaman from "../info/images/aryaman.jpg";
import Vineel from "../info/images/vineel.jpg";
import ReactPic from "../info/images/react.png";
import Flask from "../info/images/flask.png";
import Gitlab from "../info/images/gitlab.png";
import Jest from "../info/images/jest.png";
import Postgres from "../info/images/postgree.png";
import Slack from "../info/images/slack.png";
import Postman from "../info/images/postman.png";
import AWS from "../info/images/aws.png";
import Docker from "../info/images/docker.png";

export class AboutPage extends React.Component {
  constructor(props) {
    super(props);
    this.gitlabCommitsEndpoint =
      "https://gitlab.com/api/v4/projects/14463862/repository/contributors";
    this.gitlabIssuesEndpoint =
      "https://gitlab.com/api/v4/projects/14463862/issues";
    this.state = {
      userInfo: [
        {
          image: Avner,
          name: "Avner Khan",
          keyname: "avnerkhan",
          username: "avnerkhan",
          email: "avnerkhan@utexas.edu",
          description:
            "The most handsome and intelligent member of the group, you may refer to him as 'Avgod' or 'The Avdaddy'",
          role: "Frontend",
          commits: 0,
          issues: 0,
          unitTests: 6
        },
        {
          image: Kyle,
          name: "Kyle Knight",
          keyname: "Kyle Knight",
          username: "kylektex",
          email: "kylektex@cs.utexas.edu",
          description:
            "Kyle is a junior CS student at the University of Texas at Austin. He likes to watch anime because of the plot.",
          role: "Full Stack",
          commits: 0,
          issues: 0,
          unitTests: 4
        },
        {
          image: Alvaro,
          name: "Alvaro Hare",
          keyname: "Alvaro Hare",
          username: "alvarohare1",
          email: "alvaroh1@cs.utexas.edu",
          description:
            "Alvaro is a junior CS student at the University of Texas at Austin. He spends 90% of his time Googling stuff. His Alexa (and the group) call him 'Big Papi'.",
          role: "Backend",
          commits: 0,
          issues: 0,
          unitTests: 7
        },
        {
          image: Vineel,
          name: "Vineel Kodikanti",
          keyname: "vineel-k",
          username: "vineel-k",
          email: "vineel.kodikanti@utexas.edu",
          description:
            "Vineel is a junior CS student at the University of Texas at Austin. He likes gorillas. Like ALOT. He really likes gorillas.",
          role: "Backend",
          commits: 0,
          issues: 0,
          unitTests: 10
        },
        {
          image: Aryaman,
          name: "Aryaman Jadhav",
          keyname: "Aryaman Jadhav",
          username: "aryaman.jadhav",
          email: "aryaman@cs.utexas.edu",
          description:
            "Aryaman is a junior CS student at the University of Texas at Austin. Catchphrase is 'don’t REST on my APIs'",
          role: "Backend",
          commits: 0,
          issues: 0,
          unitTests: 9
        }
      ]
    };
  }

  showPeopleCard(people) {
    return (
      <Card style={{ width: "25rem", color: "black" }}>
        <Card.Img variant="top" src={people.image} />
        <Card.Header>{people.name}</Card.Header>
        <Card.Text>Description: {people.description}</Card.Text>
        <Card.Text>Role: {people.role}</Card.Text>
        <Card.Text>Commits: {people.commits}</Card.Text>
        <Card.Text>Issues: {people.issues}</Card.Text>
        <Card.Text>Unit Tests: {people.unitTests}</Card.Text>
      </Card>
    );
  }

  displayPeopleInformationCard() {
    let outerRows = [];
    const rowLimit = 3;
    for (let i = 0; i < this.state.userInfo.length; i += rowLimit) {
      let innerCols = [];
      for (let c = i; c < i + rowLimit && c < this.state.userInfo.length; c++) {
        innerCols.push(
          <Col>{this.showPeopleCard(this.state.userInfo[c])}</Col>
        );
      }
      outerRows.push(<Row>{innerCols}</Row>);
    }
    return outerRows;
  }

  writeCommitInformation(data) {
    let copyState = this.state.userInfo;
    let toCopy = {};
    for (const block of data) {
      toCopy[block["name"]] = block["commits"];
    }
    for (const person of copyState) {
      person["commits"] = toCopy[person["keyname"]];
    }
    this.setState({ userInfo: copyState });
  }

  writeData(data) {
    let copyState = this.state.userInfo;
    let countMap = {
      avnerkhan: 0,
      kylektex: 0,
      alvarohare1: 0,
      "aryaman.jadhav": 0,
      "vineel-k": 0
    };
    for (const block of data) {
      const indexer = block["author"]["username"];
      countMap[indexer] += 1;
    }

    for (let i = 0; i < copyState.length; i++) {
      const indexer = "username";
      const toIncrement = "issues";
      const currentPerson = copyState[i][indexer];
      copyState[i][toIncrement] = countMap[currentPerson];
    }
    this.setState({
      userInfo: copyState
    });
  }

  componentWillMount() {
    fetch(this.gitlabCommitsEndpoint)
      .then(res => res.json())
      .then(data => this.writeCommitInformation(data));
    fetch(this.gitlabIssuesEndpoint)
      .then(res => res.json())
      .then(data => this.writeData(data));
  }

  getCommits() {
    let commits = [];
    for (let person of this.state.userInfo) {
      commits.push(person["commits"]);
    }
    return commits;
  }

  getIssues() {
    let issues = [];
    for (let person of this.state.userInfo) {
      issues.push(person["issues"]);
    }
    return issues;
  }

  getTests() {
    let tests = [];
    for (let person of this.state.userInfo) {
      tests.push(person["unitTests"]);
    }
    return tests;
  }

  displayAllTechUsed() {
    return (
      <div>
        <Row>
          <Col>
            <TechUsed
              image={ReactPic}
              name="React"
              description="Frontend javascript framework"
            />
          </Col>
          <Col>
            <TechUsed
              image={Flask}
              name="Flask"
              description="Backend python framework"
            />
          </Col>
          <Col>
            <TechUsed
              image={Jest}
              name="Jest"
              description="Frontend testing framework"
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <TechUsed
              image={Gitlab}
              name="Gitlab"
              description="Version control"
            />
          </Col>
          <Col>
            <TechUsed
              image={Postgres}
              name="Postgres"
              description="SQL database"
            />
          </Col>
          <Col>
            <TechUsed
              image={Postman}
              name="Postman"
              description="API documentation and testing"
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <TechUsed
              image={Slack}
              name="Slack"
              description="Communication between team members"
            />
          </Col>
          <Col>
            <TechUsed
              image={AWS}
              name="Amazon Web Services"
              description="Server and frontend hosting"
            />
          </Col>
          <Col>
            <TechUsed
              image={Docker}
              name="Docker"
              description="Containerization of components"
            />
          </Col>
        </Row>
      </div>
    );
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          {this.displayPeopleInformationCard()}
          <AboutTotals
            commits={this.getCommits()}
            issues={this.getIssues()}
            tests={this.getTests()}
          />
          <a href="https://gitlab.com/avnerkhan/afterwork">Gitlab</a>
          <a href="https://blue-firefly-4988.postman.co/collections/8989792-109944b4-ea2e-4c55-9642-fd7fbed76ebf?workspace=8dc96976-3cb0-4e40-8457-f7a925dd289a">
            Postman API Docs
          </a>
          <a href="https://documenter.getpostman.com/view/3885704/SVzua1au?version=latest">
            Postman API Tests
          </a>
          <h1>Technologes Used</h1>
          {this.displayAllTechUsed()}
        </div>
      </div>
    );
  }
}

export default AboutPage;
