/* eslint-disable */

import React from "react";
import { connect } from "react-redux";
import { getTop, displayTop } from "../global";
import { changeEventInfo } from "../actions/EventInfoActions";
import { changeMeetupInfo } from "../actions/MeetupInfoActions";
import { baseServerUrl } from "../info/static";
import { Image } from "react-bootstrap";
import "../css_files/App.css";

export class OneCityPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      topEvents: [],
      topMeetups: []
    };
  }

  componentWillMount() {
    fetch(baseServerUrl + "events")
      .then(res => res.json())
      .then(data =>
        this.setState({
          topEvents: getTop(data, this.props.city.name)
        })
      );
    fetch(baseServerUrl + "meetups")
      .then(res => res.json())
      .then(data =>
        this.setState({
          topMeetups: getTop(data, this.props.city.name)
        })
      );
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <Image src={this.props.city.image} />
          <h1>{this.props.city.name}</h1>
          <h2>State: {this.props.city.region}</h2>
          <h2>
            Timezone:{" "}
            {this.props.city.timezone != null
              ? this.props.city.timezone.split("__")[1]
              : this.props.city.name}
          </h2>
          <h2>Top Events in {this.props.city.name}</h2>
          {displayTop(this.state.topEvents, true, this.props)}
          <h2>Top Meetups in {this.props.city.name}</h2>
          {displayTop(this.state.topMeetups, false, this.props)}
        </div>
      </div>
    );
  }
}

export const mapStateToProps = state => ({
  city: state.CityInfo.CityInfo
});

const mapDispatchToProps = {
  changeEventInfo,
  changeMeetupInfo
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OneCityPage);
