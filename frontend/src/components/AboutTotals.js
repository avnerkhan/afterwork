/* eslint-disable */

import React from "react";
import "../css_files/App.css";

export class AboutTotals extends React.Component {
  constructor(props) {
    super(props);
  }

  getTotalNumber(iterable) {
    let sum = 0;
    for (let i of iterable) {
      if (Number.isInteger(i)) {
        // make sure its a number
        sum += i;
      }
    }
    return sum;
  }

  render() {
    return (
      <div className="about-totals">
        <h4>Total Commits: {this.getTotalNumber(this.props.commits)}</h4>
        <h4>Total Issues: {this.getTotalNumber(this.props.issues)}</h4>
        <h4>Total Tests: {this.getTotalNumber(this.props.tests)}</h4>
      </div>
    );
  }
}

export default AboutTotals;
