import {
  CHANGE_SELECTED_CITY,
  STORE_CITY_SEARCH
} from "../types/CityInfoTypes";

const initialCityInfoState = {
  CityInfo: {
    name: "No Page Selected"
  },
  CitySearch: []
};

export function CityInfoReducer(prevState = initialCityInfoState, action) {
  const newState = Object.create(prevState, {});

  switch (action.type) {
    case CHANGE_SELECTED_CITY:
      newState.CityInfo = action.CityInfo;
      return newState;
    case STORE_CITY_SEARCH:
      newState.CitySearch = action.CitySearch;
      return newState;
    default:
      break;
  }

  return prevState;
}
