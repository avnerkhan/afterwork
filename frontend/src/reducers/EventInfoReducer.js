import {
  CHANGE_SELECTED_EVENT,
  STORE_EVENT_SEARCH
} from "../types/EventInfoTypes";

const initialEventInfoState = {
  EventInfo: {
    name: "No Page Selected"
  },
  EventSearch: []
};

export function EventInfoReducer(prevState = initialEventInfoState, action) {
  const newState = Object.create(prevState, {});

  switch (action.type) {
    case CHANGE_SELECTED_EVENT:
      newState.EventInfo = action.EventInfo;
      return newState;
    case STORE_EVENT_SEARCH:
      newState.EventSearch = action.EventSearch;
      return newState;
    default:
      break;
  }

  return prevState;
}
