import {
  CHANGE_SELECTED_MEETUP,
  STORE_MEETUP_SEARCH
} from "../types/MeetupInfoTypes";

const initialMeetupInfoState = {
  MeetupInfo: {
    name: "No Page Selected"
  },
  MeetupSearch: []
};

export function MeetupInfoReducer(prevState = initialMeetupInfoState, action) {
  const newState = Object.create(prevState, {});

  switch (action.type) {
    case CHANGE_SELECTED_MEETUP:
      newState.MeetupInfo = action.MeetupInfo;
      return newState;
    case STORE_MEETUP_SEARCH:
      newState.MeetupSearch = action.MeetupSearch;
      return newState;
    default:
      break;
  }

  return prevState;
}
