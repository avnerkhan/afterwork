Name: Alvaro Hare
EID: ah46939
GitLab ID: alvarohare1
Estimated Completion Time Phase 1: 10 hrs
Actual Completion Time Phase 1: 20 hrs
Estimated Completion Time Phase 2: 20 hrs
Actual Completion Time Phase 2: 25 hrs
Estimated Completion Time Phase 3: 15 hrs
Actual Completion Time Phase 3: 18 hrs
Estimated Completion Time Phase 4: 10 hrs
Actual Completion Time Phase 4: 8 hrs

Name: Kyle Knight
EID: ktk536
GitLab ID: kylektex
Estimated Completion Time Phase 1: 15 hrs
Actual Completion Time Phase 1: 20 hrs
Estimated Completion Time Phase 2: 20 hrs
Actual Completion Time Phase 2: 25 hrs
Estimated Completion Time Phase 3: 16 hrs
Actual Completion Time Phase 3: 18 hrs
Estimated Completion Time Phase 4: 12 hrs
Actual Completion Time Phase 4: 8 hrs


Name: Vineel Kodikanti
EID: vk5276
GitLab ID: vineel-k
Estimated Completion Time Phase 1: 25hrs
Actual Completion Time Phase 1: 20hrs
Estimated Completion Time Phase 2: 20 hrs
Actual Completion Time Phase 2: 25 hrs
Estimated Completion Time Phase 3: 20 hrs
Actual Completion Time Phase 3: 18 hrs
Estimated Completion Time Phase 4: 11 hrs
Actual Completion Time Phase 4: 7 hrs


Name: Avner Khan
EID: aak2699
GitLab ID: avnerkhan
Estimated Completion Time Phase 1:15hrs
Actual Completion Time Phase 1: 20hrs
Estimated Completion Time Phase 2: 15 hrs
Actual Completion Time Phase 2: 25 hrs
Estimated Completion Time Phase 3: 20 hrs
Actual Completion Time Phase 3: 18 hrs
Estimated Completion Time Phase 4: 10 hrs
Actual Completion Time Phase 4: 8 hrs


Name: Aryaman Jadhav
EID: arj2392
GitLab ID: aryaman.jadhav
Estimated Completion Time Phase 1: 12hrs
Actual Completion Time Phase 1: 20 hrs
Estimated Completion Time Phase 2: 22 hrs
Actual Completion Time Phase 2: 25 hrs
Estimated Completion Time Phase 3: 15 hrs
Actual Completion Time Phase 3: 18 hrs
Estimated Completion Time Phase 4: 10 hrs
Actual Completion Time Phase 4: 8 hrs


===Phase 1===
Project Leader: Avner Khan
Git SHA: aecfe76d8d216a98dd49f41cc1bda0496187581a
GitLab Pipelines: https://gitlab.com/avnerkhan/afterwork/pipelines
Website Link: https://after-work.site/

===Phase 2===
Project Leader: Kyle Knight
Git SHA: 2539e64efd7c87a975761c5d7c305f510051ead8
GitLab Pipelines: https://gitlab.com/avnerkhan/afterwork/pipelines
Website Link: https://after-work.site/
Backend Link: http://ec2-52-90-103-121.compute-1.amazonaws.com (there is nothing to be seen in root directory)

===Phase 3===
Project Leader: Alvaro Hare
Git SHA: 0448d5c0865b1671a3986877cb372acacd498d77
GitLab Pipelines: https://gitlab.com/avnerkhan/afterwork/pipelines
Website Link: https://after-work.site/
Backend Link: https://api.after-work.site/

===Phase 4===
Project Leader: Aryaman Jadhav
Git SHA: 61abbafe2021b683285a94e8e6b4cdf44f983b6f
GitLab Pipelines: https://gitlab.com/avnerkhan/afterwork/pipelines
Website Link: https://after-work.site/
Backend Link: https://api.after-work.site/
